import { LitElement, html } from "lit";
import "../emisor-evento/emisor-evento.js";
import "../receptor-evento/receptor-evento.js";

// html --> es un motor de plantilla

class GestorEvento extends LitElement {
  static get properties() {
    return {};
  }
  constructor() {
    super();
  }

  render() {
    return html` <h1>Gestor evento</h1>
      <emisor-evento @test-event="${this.proccessEvent}"></emisor-evento>
      <receptor-evento id="receiver"></receptor-evento>`;
  }

  proccessEvent(e) {
    console.log("Capturado evento del emisor");
    console.log(e);
    // DOM LOCAL. Del exterior al interior (componente) cambiamos la propiedad year y course

    this.shadowRoot.getElementById("receiver").year = e.detail.year;
    this.shadowRoot.getElementById("receiver").course = e.detail.course;
  }
}

// Añadimos el customElements
customElements.define("gestor-evento", GestorEvento);
