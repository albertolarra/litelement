import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class PersonaHeader extends LitElement {
  static get properties() {
    return {};
  }
  constructor() {
    super();
  }

  render() {
    return html` <h1>App Molona</h1> `;
  }
}

// Añadimos el customElements
customElements.define("persona-header", PersonaHeader);
