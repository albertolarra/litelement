import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class PersonaSidebar extends LitElement {
  static get properties() {
    return {
      // Los objetos se tienen que definir. Si hubiera sido un string  no era necesario
      peopleStats: { type: Object },
    };
  }
  constructor() {
    super();

    this.peopleStats = {};
  }

  render() {
    return html` <link
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
    crossorigin="anonymous"
    <aside>
        <section>
        <div>
          Hay <span>${this.peopleStats.numberOfPeople}</span> personas
        </div>
            <div class="mt-5">
                <button class="btn btn-success w-100" @click="${this.newPerson}"><strong style="font-size: 50px">+</strong>
                </button>
            </div>
        </section>
    </aside>
    `;
  }

  newPerson(e) {
    console.log("newPerson en persona-sidebar");
    console.log("se va a enganchar una nueva persona");

    this.dispatchEvent(new CustomEvent("new-person", {}));
  }
}

// Añadimos el customElements
customElements.define("persona-sidebar", PersonaSidebar);
