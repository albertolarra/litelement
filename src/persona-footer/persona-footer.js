import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class PersonaFooter extends LitElement {
  static get properties() {
    return {};
  }
  constructor() {
    super();
  }

  render() {
    return html` <h5>Footer</h5> `;
  }
}

// Añadimos el customElements
customElements.define("persona-footer", PersonaFooter);
