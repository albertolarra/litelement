import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class ReceptorEvento extends LitElement {
  static get properties() {
    return {
      course: { Type: String },
      year: { Type: String },
    };
  }
  constructor() {
    super();
  }

  render() {
    return html` <h3>Receptor evento</h3>
      <h5>Este curso es de ${this.course}</h5>
      <h5>y estamos en el año ${this.year}</h5>`;
  }
}

// Añadimos el customElements
customElements.define("receptor-evento", ReceptorEvento);
