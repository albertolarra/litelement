import { LitElement, html } from "lit";
import "../persona-ficha-listado/persona-ficha-listado.js";
import "../persona-form/persona-form.js";
import "../persona-main-dm/persona-main-dm.js";

// html --> es un motor de plantilla

class PersonaMain extends LitElement {
  static get properties() {
    return {
      people: { type: Array },
      // aqui se pone la propiedad porque asi la hace observable, ¡¡MUY IMPORTANTE!! Si no es observable luego no se puede tratar
      showPersonForm: { type: Boolean },
    };
  }
  constructor() {
    super();

    this.people = [];

    this.showPersonForm = false;
  }

  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />
      <h2 class="text-center">Personas</h2>
      <div class="row" id="peopleList">
        <main class="row row-cols-1 row-cols-sm-4">
          ${this.people.map(
            (person) =>
              html`<persona-ficha-listado
                fname="${person.name}"
                yearsInCompany="${person.yearsInCompany}"
                .photo="${person.photo}"
                profile="${person.profile}"
                @delete-person="${this.deletePerson}"
                @info-person="${this.infoPerson}"
              ></persona-ficha-listado>`
          )}
        </main>
      </div>
      <div class="row">
        <persona-form
          id="personForm"
          class="d-none border rounded border-primary"
          @persona-form-close="${this.personFormClose}"
          @persona-form-store="${this.personFormStore}"
        ></persona-form>
        <persona-main-dm
          @updated-people="${this.updatedPeople}"
        ></persona-main-dm>
      </div>
    `;
  }

  // Ciclo de vida de LitElement. El updated se va a ejecutar siempre que se cambia el valor de una propiedad
  updated(changedProperties) {
    console.log("updated en persona-main");

    if (changedProperties.has("showPersonForm")) {
      console.log(
        "Has cambiado el valor de la propiedad showPersonForm en persona-main"
      );
      if (this.showPersonForm === true) {
        this.showPersonFormData();
      } else {
        this.showPersonList();
      }
    }

    if (changedProperties.has("people")) {
      console.log(
        "Ha cambiado el valor de la propiedad people en persona-main"
      );

      this.dispatchEvent(
        new CustomEvent("people-updated", {
          detail: {
            people: this.people,
          },
        })
      );
    }
  }

  deletePerson(e) {
    console.log("estamos en deletePerson persona-main");
    console.log("se va a borrar la persona con nombre " + e.detail.name);

    this.people = this.people.filter((person) => person.name != e.detail.name);
  }

  infoPerson(e) {
    console.log("estamos en infoPerson persona-main");
    console.log("se ha pedido mas informacion de la persona " + e.detail.name);

    let chosenPerson = this.people.filter(
      (person) => person.name === e.detail.name
    );

    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;

    this.shadowRoot.getElementById("personForm").person = person;
    this.shadowRoot.getElementById("personForm").editingPerson = true;

    this.showPersonForm = true;
  }

  showPersonList() {
    console.log("showPersonList");
    console.log("Mostrar el listado de personas");

    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
  }

  showPersonFormData() {
    console.log("showPersonFormData");
    console.log("Mostrar el formulario de personas");

    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
  }

  personFormClose(e) {
    console.log("personFormClose");

    this.showPersonForm = false; // De esta forma cambia la propiedad y se ejecuta el updated
  }

  personFormStore(e) {
    console.log("personFormStore");
    console.log(e.detail.person);

    if (e.detail.editingPerson === true) {
      console.log(
        "Se va a actualizar la persona de nombre " + e.detail.person.name
      );

      // let indexOfPerson = this.people.findIndex(
      //   (person) => person.name === e.detail.person.name
      // );
      // if (indexOfPerson >= 0) {
      //   console.log("Persona encontrado");

      //   this.people[indexOfPerson] = e.detail.person;

      // Actualizamos código para que el updated de persona-form.js se llame al hacer la modificación
      this.people = this.people.map((person) =>
        person.name === e.detail.person.name
          ? (person = e.detail.person)
          : person
      );
    } else {
      console.log("Se va a almacenar una persona nueva");
      // this.people.push(e.detail.person);
      // todos los elementos del array y se añade el último al final . Spread syntax
      // Actualizamos código para que el updated de persona-form.js se llame al hacer la creación de la persona nueva
      this.people = [...this.people, e.detail.person];
    }

    console.log("proceso terminado");

    this.showPersonForm = false;
  }

  updatedPeople(e) {
    console.log("updatedPeople en persona main");
    console.log(e.detail);
    this.people = e.detail.people;
  }
}

// Añadimos el customElements
customElements.define("persona-main", PersonaMain);
