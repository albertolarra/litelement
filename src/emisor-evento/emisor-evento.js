import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class EmisorEvento extends LitElement {
  static get properties() {
    return {};
  }
  constructor() {
    super();
  }

  render() {
    return html`
      <h3>Emisor evento</h3>
      <button @click="${this.sendEvent}">No pulsar</button>
    `;
  }
  sendEvent(e) {
    console.log("sendEvent");
    console.log(e);

    this.dispatchEvent(
      //Evento custom, se captura luego en gestor-evento
      new CustomEvent("test-event", {
        detail: {
          course: "TechU",
          year: 2022,
        },
      })
    );
  }
}

// Añadimos el customElements
customElements.define("emisor-evento", EmisorEvento);
