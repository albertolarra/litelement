import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class FichaPersona extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      yearsInCompany: { type: Number },
      personInfo: { type: String }, // Propiedad calculada, en base a otro campo
    };
  }

  constructor() {
    super();
    this.name = "Alberto";
    this.yearsInCompany = 12;
  }

  // Esto es litElement al 100%
  // updated: cuando ya se ha actualizado y se ha pintado en el DOM
  updated(changedProperties) {
    // valor - nombre
    // console.log("changedProperties: " + changedProperties);
    changedProperties.forEach((oldValue, propName) => {
      console.log(
        "Propiedad " +
          propName +
          " ha cambiado de valor, anterior era " +
          oldValue
      );
    });
    if (changedProperties.has("name")) {
      console.log(
        "Propiedad name cambia valor, anterior era " +
          changedProperties.get("name") +
          " --> nuevo es " +
          this.name
      );
    }

    if (changedProperties.has("yearsInCompany")) {
      console.log(
        "Propiedad name cambia valor, anterior era " +
          changedProperties.get("yearsInCompany") +
          " --> nuevo es " +
          this.yearsInCompany
      );
      this.updatePersonInfo();
    }
  }

  render() {
    return html`
    <div>
      <label>Nombre Completo</label>
      <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
      <br />
      <label>Años en la empresa</label>
      <input type="text" value="${this.yearsInCompany}"  @input="${this.updateYearsInCompany}"></input>
      <br />
      <input type="text" value="${this.personInfo}" disabled></input>
      <br />
    </div> 
    `;
  }

  updateName(e) {
    console.log("updateName");
    console.log(e);
    // asi hacemos el two ways en el binding
    this.name = e.target.value;
  }

  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    this.yearsInCompany = e.target.value;
  }

  updatePersonInfo() {
    if (this.yearsInCompany >= 7) {
      this.personInfo = "lead";
    } else if (this.yearsInCompany >= 5) {
      this.personInfo = "senior";
    } else if (this.yearsInCompany >= 3) {
      this.personInfo = "team";
    } else {
      this.personInfo = "junior";
    }
  }
}

// Añadimos el customElements
customElements.define("ficha-persona", FichaPersona);
