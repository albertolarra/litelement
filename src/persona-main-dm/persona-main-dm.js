import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class PersonaMainDm extends LitElement {
  static get properties() {
    return {
      people: { type: Array },
    };
  }
  constructor() {
    super();

    this.people = [
      {
        name: "Bart Simpson",
        yearsInCompany: 10,
        photo: {
          src: "./img/BartSimpson.jpg",
          alt: "Bart Simpson",
        },
        profile: "Lorem Ipsum",
      },
      {
        name: "Kylian Mbappe",
        yearsInCompany: 2,
        photo: {
          src: "./img/Mbappe.jpg",
          alt: "Kylian Mbappe",
        },
        profile: "profile 2",
      },
      {
        name: "David Guetta",
        yearsInCompany: 5,
        photo: {
          src: "./img/DavidGuetta.jpg",
          alt: "David Guetta",
        },
        profile: "profile 3",
      },
      {
        name: "Homer Simpson",
        yearsInCompany: 10,
        photo: {
          src: "./img/BartSimpson.jpg",
          alt: "Homer Simpson",
        },
        profile: "profile 4",
      },
      {
        name: "Erwing Haaland",
        yearsInCompany: 10,
        photo: {
          src: "./img/Mbappe.jpg",
          alt: "Erwing Haaland",
        },
        profile: "profile 5",
      },
    ];
  }

  updated(changedProperties) {
    console.log("updated en persona-main-dm");

    if (changedProperties.has("people")) {
      console.log(
        "Ha cambiado el valor de la propiedad people en persona-main-dm"
      );

      this.dispatchEvent(
        new CustomEvent("updated-people", {
          detail: {
            people: this.people,
          },
        })
      );
    }
  }
}

// Añadimos el customElements
customElements.define("persona-main-dm", PersonaMainDm);
