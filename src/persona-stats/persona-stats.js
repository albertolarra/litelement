import { LitElement } from "lit";

// html --> es un motor de plantilla

class PersonaStats extends LitElement {
  static get properties() {
    return {
      people: { Type: Array },
    };
  }
  constructor() {
    super();

    // En el constructor se inicializa y por eso se llama a updated
    this.people = [];
  }

  // changedProperties es el valor anterior antes del cambio. EL valor actual está en la propia propiedad
  updated(changedProperties) {
    console.log("updated en persona-stats");
    if (changedProperties.has("people")) {
      console.log(
        "Ha cambiado el valor de la propiedad people en persona stats"
      );

      let peopleStats = this.gatherPeopleArrayInfo(this.people);

      this.dispatchEvent(
        new CustomEvent("updated-people-stats", {
          detail: {
            peopleStats: peopleStats,
          },
        })
      );
    }
  }

  gatherPeopleArrayInfo(people) {
    console.log("gatherPeopleArrayInfo");
    let peopleStats = {};
    peopleStats.numberOfPeople = people.length;

    console.log(peopleStats);

    return peopleStats;
  }

  // Este componente solo va a tener logica de negocio. NO TIENE RENDER
  // Componente que va a hacer calculos con el array de persona
  //   render() {
  //     return html` <h1>Persona stats</h1> `;
  //   }
}

// Añadimos el customElements
customElements.define("persona-stats", PersonaStats);
