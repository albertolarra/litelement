import { LitElement, html } from "lit";

// html --> es un motor de plantilla

class HolaMundo extends LitElement {
  render() {
    return html` <div>Hola Mundo!</div> `;
  }
}

// Añadimos el customElements
customElements.define("hola-mundo", HolaMundo);
